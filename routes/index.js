var express = require('express');
var router = express.Router();
var express = require('express');
var app = express();
var server = require('http').Server(app);
var repositoryService = require('../services/repositoryService.js')


const io = require("socket.io")(server, {
	cors: {
		origin: '*',
	}
});

const cors = require('cors');
const { exception } = require('console');

router.use(cors());



server.listen(3000, async () => {
	console.log('Servidor corriendo en http://localhost:3000');
	await repositoryService.init()
	console.log('Iniciando Repositorio')
});

io.on('connection', function (socket) {

	socket.emit('repos-info', JSON.stringify(repositoryService.getAllReposInfo())) //Devolvemos el repository

	socket.on('repo-clone', repoId => {
		console.log("Cloning Repo")
		socket.emit('repo-clone', JSON.stringify(repositoryService.findRepoById(repoId)))
	})

	socket.on('repos-info', repoId => {
		socket.broadcast.emit('repos-info', JSON.stringify(repositoryService.getAllReposInfo()))
	})

	socket.on('repo-commit', async changes => {

		try {
			const appliedChanges = await repositoryService.applyChangeToRepo(changes)
			socket.broadcast.emit('repo-synchro', changes)
		}
		catch(patchException) {
			console.log("Hubo un error");
			console.log(patchException);
			socket.emit('restore-repository', JSON.stringify(repositoryService.findRepoById({id: JSON.parse(changes).id})))
		}
	})
});
/* GET home page. */
router.get('/', function (req, res, next) {
	res.render('index', { title: 'Express' });
});

router.post('/dashboard', async (req, res, next) => {
	let newDashboard = req.body;
	newDashboard = await repositoryService.createDashboard(newDashboard)
	res.json(newDashboard)
});

router.get("/healthcheck", (req, res, next) => {
	res.status(200).json({"status": "up"})
})

router.get("/crash", (req, res, next) => {
	process.nextTick(function () {
		throw new Error;
	  });
})

module.exports = router;
