const jsonpatch = require('fast-json-patch')
const fs = require('fs').promises;

let repositoryIds = 1

let repositories = []

// const repositories = [
//     {
//         id: 0,
//         author: "Mock",
//         name : "Mi Dashboard 0",
//         repository:{cards: []}
//     }
// ]

// repositories[0].repository.cards.push({
// 	index: 0,
// 	title: 'Mock Card',
// 	task: "Mock Task",
// 	status: "To-do",
// 	metadata: {
// 		author: 'Backend',
// 		time: Date.now()
// 	}
// })
// repositories[0].repository.cards.push({
// 	index: 0,
// 	title: 'Mock Card 2',
// 	task: "Mock Task",
// 	status: "In-Progress",
// 	metadata: {
// 		author: 'Backend',
// 		time: Date.now()
// 	}
// })
// repositories[0].repository.cards.push({
// 	index: 0,
// 	title: 'Mock Card 3',
// 	task: "Mock Task",
// 	status: "Done",
// 	metadata: {
// 		author: 'Backend',
// 		time: Date.now()
// 	}
// })


const applyChangeToRepo = async changes => {
	changes = JSON.parse(changes)
    const repo = repositories.find(r => r.id == changes.id)
    repo.repository.cards  = jsonpatch.applyPatch(repo.repository.cards, changes.changes, true, false).newDocument
	await syncToFile()
    return changes
}

const getAllReposInfo = () => {
    return repositories.map(r => {
        return {
            ...r,
            repository: null
        }
    })
}

const findRepoById = repoId => repositories.find(r => r.id === repoId.id)

const createDashboard = async newDashboard => {
	const dash = {
		...newDashboard,
		id: repositoryIds++,
		repository:{cards: []}
	}

	repositories.push(
		dash
	)
	await syncToFile()
	return dash
}

const syncToFile = async () => {
	return fs.writeFile('./db/repositoryData.json', JSON.stringify({repositoryIds, repositories})); // need to be in an async function
}

const init = async() => {
	let data = await fs.readFile('./db/repositoryData.json');
	data = JSON.parse(data)
	repositoryIds = data.repositoryIds
	repositories = data.repositories
}

module.exports = {
    applyChangeToRepo,
    getAllReposInfo,
    findRepoById,
	createDashboard,
	init
    
}